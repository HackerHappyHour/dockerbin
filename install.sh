#!/usr/bin/env bash
# special thanks to creationix, for which much of this installer
# was borred from. https://github.com/creationix/dockerbin

{ #AllTheScriptFTW

dockerbin_has(){
	type "$1" > /dev/null 2>&1
}

dockerbin_install_dir(){
	echo "${DOCKERBIN_DIR:-"$HOME/.dockerbin"}"
}

dockerbin_latest_version(){
	echo "v1.0.0"
}

dockerbin_source(){
	echo "https://gitlab.com/HackerHappyHour/dockerbin.git"
}

dockerbin_download(){
  if dockerbin_has "curl"; then
    curl -q "$@"
  elif dockerbin_has "wget"; then
    # Emulate curl with wget
    ARGS=$(echo "$*" | command sed -e 's/--progress-bar /--progress=bar /' \
                           -e 's/-L //' \
                           -e 's/-I /--server-response /' \
                           -e 's/-s /-q /' \
                           -e 's/-o /-O /' \
                           -e 's/-C - /-c /')
    # shellcheck disable=SC2086
    eval wget $ARGS
  fi
}

install_dockerbin_from_git() {
  local INSTALL_DIR
  INSTALL_DIR="$(dockerbin_install_dir)"

  if [ -d "$INSTALL_DIR/.git" ]; then
    echo "=> dockerbin is already installed in $INSTALL_DIR, trying to update using git"
    command printf "\r=> "
    command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" fetch 2> /dev/null || {
      echo >&2 "Failed to update dockerbin, run 'git fetch' in $INSTALL_DIR yourself."
      exit 1
    }
  else
    # Cloning to $INSTALL_DIR
    echo "=> Downloading dockerbin from git to '$INSTALL_DIR'"
    command printf "\r=> "
    mkdir -p "${INSTALL_DIR}"
    if [ "$(ls -A "${INSTALL_DIR}")" ]; then
      command git init "${INSTALL_DIR}" || {
        echo >&2 'Failed to initialize dockerbin repo. Please report this!'
        exit 2
      }
      command git --git-dir="${INSTALL_DIR}/.git" remote add origin "$(dockerbin_source)" 2> /dev/null \
        || command git --git-dir="${INSTALL_DIR}/.git" remote set-url origin "$(dockerbin_source)" || {
        echo >&2 'Failed to add remote "origin" (or set the URL). Please report this!'
        exit 2
      }
      command git --git-dir="${INSTALL_DIR}/.git" fetch origin --tags || {
        echo >&2 'Failed to fetch origin with tags. Please report this!'
        exit 2
      }
    else
      command git clone "$(dockerbin_source)" "${INSTALL_DIR}" || {
        echo >&2 'Failed to clone dockerbin repo. Please report this!'
        exit 2
      }
    fi
  fi
  command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" checkout -f --quiet "$(dockerbin_latest_version)"
  if [ ! -z "$(command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" show-ref refs/heads/master)" ]; then
    if command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" branch --quiet 2>/dev/null; then
      command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" branch --quiet -D master >/dev/null 2>&1
    else
      echo >&2 "Your version of git is out of date. Please update it!"
      command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" branch -D master >/dev/null 2>&1
    fi
  fi

  echo "=> Compressing and cleaning up git repository"
  if ! command git --git-dir="$INSTALL_DIR"/.git --work-tree="$INSTALL_DIR" gc --aggressive --prune=now ; then
      echo >&2 "Your version of git is out of date. Please update it!"
  fi
  return
}

#
# Instructions on downloading docker
#

dockerbin_install_docker(){
	echo "coming soon... auto install of docker?"
}

install_dockerbin_as_script(){
	local INSTALL_DIR
	INSTALL_DIR="$(dockerbin_install_dir)"
	local DOCKERBIN_SOURCE_LOCAL
	DOCKERBIN_SOURCE_LOCAL=$(dockerbin_source script)

	# downloading to $INSTALL_DIR
	mkdir -p "$INSTALL_DIR"
	if [ -f "$INSTALL_DIR/dockerbin.sh" ]; then
		echo "=> dockerbin is already installed in $INSTALL_DIR, attempting to update..."	
	else
		echo "=> Downloading dockerbin as script to '$INSTALL_DIR'"
	fi

	dockerbin_downoad -s "$DOCKERBIN_SOURCE_LOCAL" -o "$INSTALL_DIR/dockerbin.sh" || {
		echo >&2 "Failed to download '$DOCKERBIN_SOURCE_LOCAL'"
	}

	chmod a+x "$INSTALL_DIR/bin" || {
		echo >&2 "Failed to mark '$INSTALL_DIR/bin' as executable"
	}
}

detect_profile() {
  if [ -n "${PROFILE}" ] && [ -f "${PROFILE}" ]; then
    echo "${PROFILE}"
		return
	fi

	local SHELL_PROFILE
	SHELL_PROFILE=''
	local SHELLTYPE
	SHELLTYPE="$(basename "/$SHELL")"

  if [ "$SHELLTYPE" = "bash" ]; then
    if [ -f "$HOME/.bashrc" ]; then
      SHELL_PROFILE="$HOME/.bashrc"
    elif [ -f "$HOME/.bash_profile" ]; then
      SHELL_PROFILE="$HOME/.bash_profile"
    fi
  elif [ "$SHELLTYPE" = "zsh" ]; then
    SHELL_PROFILE="$HOME/.zshrc"
  fi

  if [ -z "$SHELL_PROFILE" ]; then
    if [ -f "$HOME/.profile" ]; then
      SHELL_PROFILE="$HOME/.profile"
    elif [ -f "$HOME/.bashrc" ]; then
      SHELL_PROFILE="$HOME/.bashrc"
    elif [ -f "$HOME/.bash_profile" ]; then
      SHELL_PROFILE="$HOME/.bash_profile"
    elif [ -f "$HOME/.zshrc" ]; then
      SHELL_PROFILE="$HOME/.zshrc"
    fi
  fi

  if [ ! -z "$SHELL_PROFILE" ]; then
    echo "$SHELL_PROFILE"
  fi
}

dockerbin_do_install() {
  if [ -z "${METHOD}" ]; then
    # Autodetect install method
    if dockerbin_has git; then
      install_dockerbin_from_git
    elif dockerbin_has dockerbin_download; then
      install_dockerbin_as_script
    else
      echo >&2 'You need git, curl, or wget to install dockerbin'
      exit 1
    fi
  elif [ "${METHOD}" = 'git' ]; then
    if ! dockerbin_has git; then
      echo >&2 "You need git to install dockerbin"
      exit 1
    fi
    install_dockerbin_from_git
  elif [ "${METHOD}" = 'script' ]; then
    if ! dockerbin_has dockerbin_download; then
      echo >&2 "You need curl or wget to install dockerbin"
      exit 1
    fi
    install_dockerbin_as_script
  fi

  echo

  local DOCKERBIN_PROFILE
  DOCKERBIN_PROFILE="$(dockerbin_detect_profile)"
  local INSTALL_DIR
  INSTALL_DIR="$(dockerbin_install_dir)"

  SOURCE_STR="\nexport DOCKERBIN_DIR=\"$INSTALL_DIR\"\n[ -s \"\$DOCKERBIN_DIR/dockerbin.sh\" ] && . \"\$DOCKERBIN_DIR/dockerbin.sh\"  # This loads dockerbin\n"

  if [ -z "${DOCKERBIN_PROFILE-}" ] ; then
    echo "=> Profile not found. Tried ${DOCKERBIN_PROFILE} (as defined in \$PROFILE), ~/.bashrc, ~/.bash_profile, ~/.zshrc, and ~/.profile."
    echo "=> Create one of them and run this script again"
    echo "=> Create it (touch ${DOCKERBIN_PROFILE}) and run this script again"
    echo "   OR"
    echo "=> Append the following lines to the correct file yourself:"
    command printf "${SOURCE_STR}"
  else
    if ! command grep -qc '/dockerbin.sh' "$DOCKERBIN_PROFILE"; then
      echo "=> Appending source string to $DOCKERBIN_PROFILE"
      command printf "$SOURCE_STR" >> "$DOCKERBIN_PROFILE"
    else
      echo "=> Source string already in ${DOCKERBIN_PROFILE}"
    fi
  fi

  # Source dockerbin
  # shellcheck source=/dev/null
  . "${INSTALL_DIR}/dockerbin.sh"

	dockerbin_install_docker

  echo "=> Close and reopen your terminal to start using dockerbin or run the following to use it now:"
  command printf "$SOURCE_STR"
}

[ "_$DOCKERBIN_ENV" = "_testing" ] || dockerbin_do_install
} #AllTheScriptFTW
