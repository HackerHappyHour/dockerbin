**Archived:** Another promising project with this purpose has come to our attention,
and we will not be developing this project further.  

Go take a peek at [Whalebrew](https://github.com/brfirsh/whalebrew)

# dockerbin

_**Note:** Project is unreleased, and the features below are not yet implemented. This doc is
serving as a spec for the release target `v1.0.0`. As features become implemented, A ✓ will
be added to the section describing the feature._

This projects adds an executable path for alias scripts that execute docker images
instead of binaries.  For example, I can use the longlivechief/packer docker image
instead of installing packer natively, but then I have to use a unweildy command.

This project will turn this:

```bash
docker run --rm -it -v `pwd` docker/image <command>
```

Back into this:

```bash
<command>
```

## Installation

```bash
git clone git@gitlab.com:HackerHappyHour/dockerbin $HOME/.dockerbin
chmod +x $HOME/.dockerbin/install.sh
sh $HOME/.dockerbin/install.sh
```

## Usage

In order to avoid collisions between the docker-run programs from their natively
installed counterparts, dockerbins are called by adding a `d` to the name of the command.
You can also use the long-form of the dockerbin command, which prepends `dbin-` to
the name of the command. Commands that already end in `d` will only be available in their
long-forms.

For example:

| Command | dockerbin short-form | dockerbin long-form |
| ------- | :------------------: | :-----------------: |
| packer  | packerd | dbin-packer |
| node    | noded   | dbin-node   |
| npm     | npmd    | dbin-npm    |
| devd    | N/A     | dbin-devd   |

If you wish to use a specific version/tag of your command, append `:tag` to the command.
For example, to use a specific version of packer:

    packerd:0.9 <subcommand>

## How it works

Each `dockerbin` compatible project will have a dockerbin/command.sh script, that
will essentially be the same as aliasing the original command name to the `docker run`
command, in order to emulate native usage.

### Bin management

dockerbin works similarly to `brew`, with a little bit of the popular NVM thrown
in. It has a bucket of available commands, (docker run aliases) that it adds to your
`$PATH`. The NVM part comes in, because you can enable specific versions of a command.
However unline NVM, multiple versions of a command can be installed
on the system simultaneously, thanks to docker.

*Note: If you would like to add to the list of supported dockerbins, submit a 
Pull Request to this project. (see CONTRIBUTING).*

Unlike brew and NVM, you do not need to install each program before using it. Since dockerbin
uses docker containers, the usage of the command will detect if the appropriate image
exists on your system, and will pull it from your registry if it doesn't exist.

